import {getArgs} from "./helpers/args.mjs";
import {messageError, messageHelp, messageSuccess} from "./service/log.service.js";
import {weatherData, yamlData} from "./service/data.service.js";

const argsInit = async () => {
    let args = getArgs(process.argv);
    // help
    if (args.includes('-h')) {
        messageHelp();
    }
    // стандартный вывод с информацией
    else if (args[0] === undefined && args[1] === undefined) {
        const dataApi = await weatherData()
        if (dataApi !== null) {
            messageSuccess(`Погода в городе ${dataApi.nameCity}
${dataApi.descriptionWeather}
Температура: ${dataApi.temp} (Ощущается как ${dataApi.feelsLike})
Влажность: ${dataApi.humidity}
Скорость ветра: ${dataApi.windSpeed} (Порыв: ${dataApi.windGust})`);
        } else {
            messageError('Неверно указан токен');
        }
    }
    // установление города
    else if (args[0] === '-s') {
        if (args[1] !== undefined) {
            const cityArgs = args[1];
            yamlData("city", cityArgs);
        } else {
            messageError(`Вводите город в формате "-s город"!`);
        }
    } else if (args[0] === '-t') {
        if (args[1] !== undefined) {
            const tokenArgs = args[1];
            yamlData("token", tokenArgs);
        } else {
            messageError(`Вводите ключ в формате "-t ключ"!
Ключ можно создать по адресу "https://home.openweathermap.org/api_keys"`);
        }
    }
};

(async () => {
    await argsInit();
})();
