const getArgs = (args) => {
    const value1 = args[2];
    const value2 = args[3];
    return [value1, value2];
}

export {getArgs}