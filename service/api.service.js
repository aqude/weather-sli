import axios from "axios";
import {yamlData} from "./data.service.js";
import {messageError} from "./log.service.js";

// Функция для получения данных о погоде
const getWeatherData = async () => {
    const data = await yamlData()
    const apiKey =  data.token;
    const city = data.city
        try {

            // Отправка GET-запроса к API
            const response = await axios.get(`https://api.openweathermap.org/data/2.5/weather`, {
                params: {
                    q: city,
                    appid: apiKey,
                    units: 'metric',
                    lang: 'ru'
                }
            });
            // Возвращаем полученные данные
            return response.data;
        } catch (e) {
            if (e?.response?.status === 404) {
                messageError('Неверно указан город');
            } else if (e?.response?.status === 401) {
                messageError('Неверно указан токен');
            } else {
                messageError(e.message);
            }
        }
};

export {getWeatherData}