import chalk from "chalk";
import dedent from "dedent"
const messageError = (message) => {
    console.log(chalk.bgRed(' ERROR ') + '\n' + message);
}
const messageSuccess = (message) => {
    console.log(chalk.bgGreen(' SUCCESS ') + '\n' + message);
}
const messageHelp = () => {
    console.log(
        dedent`${chalk.bgCyan(' HELP ')}
        Используйте одно из команд:
        - : Вывод погоды;
        -h: Помощь;
        -s: [CITY];
        -t: [API_KEY];
        `);
}

export {messageError, messageSuccess, messageHelp}