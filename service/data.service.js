import fs from "fs";
import yaml from "js-yaml";
import {getWeatherData} from "./api.service.js";

const filePath = './data/data.yaml';
const yamlParser = async (item_, value) => {
    try {
        let data = await fs.promises.readFile(filePath, "utf8");
        let loadedYaml = yaml.load(data);
        if (value !== undefined) {
            loadedYaml[item_] = value;
            const updatedYamlData = yaml.dump(loadedYaml);
            await fs.promises.writeFile(filePath, updatedYamlData, "utf8");
            console.log("Данные успешно занесены.");
        } else {

            return loadedYaml[item_]
        }
    } catch (error) {
        console.error("Ошибка при парсинге YAML:", error);
        return null
    }
}


const yamlData = async (data, value) => {
// token or city
    if (value !== undefined) {
        await yamlParser(data, value);
    } else {
        let city = await yamlParser('city');
        let token = await yamlParser('token')

        return {
            city,
            token
        }
    }
}

const weatherData = async () => {
    const data = await getWeatherData();

    let temp = data.main.temp;
    let feelsLike = data.main.feels_like;
    let humidity = data.main.humidity;
    let descriptionWeather = data.weather[0].description;
    let nameCity = data.name;
    let windSpeed = data.wind.speed
    let windGust = data.wind.gust// порыв ветра

    return {
        temp,
        feelsLike,
        humidity,
        descriptionWeather,
        nameCity,
        windSpeed,
        windGust,
    }
}

export {yamlData, weatherData}